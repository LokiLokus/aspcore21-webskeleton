using System.ComponentModel.DataAnnotations;
using WebSkeleton.Filter;

namespace WebSkeleton.RequestModel {
	public class LoginModel {
		[Required(ErrorMessage = "Not valid UserName")]
		public string UserName { get; set; }

		[Required(ErrorMessage = "Not valid Password")]
		public string Password { get; set; }
	}
}
namespace WebSkeleton.Service.Logger {
	public enum LogLevel {
		Verbose,Debug,Information,Warning,Error,Critical
	}
}
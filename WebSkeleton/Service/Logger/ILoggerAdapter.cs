namespace WebSkeleton.Service.Logger {
	public interface ILoggerAdapter {
		void Write(LogLevel logLevel, string message, string className, string methodName, int line, params object[] objects);
	}
}
using System;
using System.Runtime.CompilerServices;
using WebSkeleton.Service.Logger.Adapter;

namespace WebSkeleton.Service.Logger {
	public static class Logger {
		private static ILoggerAdapter _logger { get; set; }

		static Logger()
		{
			_logger = new BasicLoggerAdapter();
		}

		public static void SetLogger(ILoggerAdapter loggerAdapter)
		{
			_logger = loggerAdapter ?? throw new LoggerAdapterIsNullException();
		}

		#region Verbose
		public static void Verbose(string data,object ob1,object obj2,object obj3, [CallerLineNumber] int lineNr = 0, [CallerMemberName] string callerName = "",[CallerFilePath] string className = "")
		{
			_logger.Write(LogLevel.Verbose,data,callerName,className,lineNr,ob1,obj2,obj3);
		}
		
		public static void Verbose(string data,object ob1,object obj2,[CallerLineNumber] int lineNr = 0, [CallerMemberName] string callerName = "",[CallerFilePath] string className = "")
		{
			_logger.Write(LogLevel.Verbose,data,callerName,className,lineNr,ob1,obj2);
		}
		
		public static void Verbose(string data,object ob1,[CallerLineNumber] int lineNr = 0, [CallerMemberName] string callerName = "",[CallerFilePath] string className = "")
		{
			_logger.Write(LogLevel.Verbose,data,callerName,className,lineNr,ob1);
		}
		
		public static void Verbose(string data, [CallerLineNumber] int lineNr = 0,[CallerMemberName] string callerName = "",[CallerFilePath] string className = "")
		{
			_logger.Write(LogLevel.Verbose,data,callerName,className,lineNr);
		}
#endregion
		
		#region Debug
		public static void Debug(string data,object ob1,object obj2,object obj3, [CallerLineNumber] int lineNr = 0, [CallerMemberName] string callerName = "",[CallerFilePath] string className = "")
		{
			_logger.Write(LogLevel.Debug,data,callerName,className,lineNr,ob1,obj2,obj3);
		}
		
		public static void Debug(string data,object ob1,object obj2,[CallerLineNumber] int lineNr = 0, [CallerMemberName] string callerName = "",[CallerFilePath] string className = "")
		{
			_logger.Write(LogLevel.Debug,data,callerName,className,lineNr,ob1,obj2);
		}
		
		public static void Debug(string data,object ob1,[CallerLineNumber] int lineNr = 0, [CallerMemberName] string callerName = "",[CallerFilePath] string className = "")
		{
			_logger.Write(LogLevel.Debug,data,callerName,className,lineNr,ob1);
		}
		
		public static void Debug(string data, [CallerLineNumber] int lineNr = 0,[CallerMemberName] string callerName = "",[CallerFilePath] string className = "")
		{
			_logger.Write(LogLevel.Debug,data,callerName,className,lineNr);
		}
		#endregion

		#region Information
		public static void Information(string data,object ob1,object obj2,object obj3, [CallerLineNumber] int lineNr = 0, [CallerMemberName] string callerName = "",[CallerFilePath] string className = "")
		{
			_logger.Write(LogLevel.Information,data,callerName,className,lineNr,ob1,obj2,obj3);
		}
		
		public static void Information(string data,object ob1,object obj2,[CallerLineNumber] int lineNr = 0, [CallerMemberName] string callerName = "",[CallerFilePath] string className = "")
		{
			_logger.Write(LogLevel.Information,data,callerName,className,lineNr,ob1,obj2);
		}
		
		public static void Information(string data,object ob1,[CallerLineNumber] int lineNr = 0, [CallerMemberName] string callerName = "",[CallerFilePath] string className = "")
		{
			_logger.Write(LogLevel.Information,data,callerName,className,lineNr,ob1);
		}
		
		public static void Information(string data, [CallerLineNumber] int lineNr = 0,[CallerMemberName] string callerName = "",[CallerFilePath] string className = "")
		{
			_logger.Write(LogLevel.Information,data,callerName,className,lineNr);
		}
		#endregion

		#region Warning
		public static void Warning(string data,object ob1,object obj2,object obj3, [CallerLineNumber] int lineNr = 0, [CallerMemberName] string callerName = "",[CallerFilePath] string className = "")
		{
			_logger.Write(LogLevel.Warning,data,callerName,className,lineNr,ob1,obj2,obj3);
		}
		
		public static void Warning(string data,object ob1,object obj2,[CallerLineNumber] int lineNr = 0, [CallerMemberName] string callerName = "",[CallerFilePath] string className = "")
		{
			_logger.Write(LogLevel.Warning,data,callerName,className,lineNr,ob1,obj2);
		}
		
		public static void Warning(string data,object ob1,[CallerLineNumber] int lineNr = 0, [CallerMemberName] string callerName = "",[CallerFilePath] string className = "")
		{
			_logger.Write(LogLevel.Warning,data,callerName,className,lineNr,ob1);
		}
		
		public static void Warning(string data, [CallerLineNumber] int lineNr = 0,[CallerMemberName] string callerName = "",[CallerFilePath] string className = "")
		{
			_logger.Write(LogLevel.Warning,data,callerName,className,lineNr);
		}
		#endregion

		#region Error
		public static void Error(string data,object ob1,object obj2,object obj3, [CallerLineNumber] int lineNr = 0, [CallerMemberName] string callerName = "",[CallerFilePath] string className = "")
		{
			_logger.Write(LogLevel.Error,data,callerName,className,lineNr,ob1,obj2,obj3);
		}
		
		public static void Error(string data,object ob1,object obj2,[CallerLineNumber] int lineNr = 0, [CallerMemberName] string callerName = "",[CallerFilePath] string className = "")
		{
			_logger.Write(LogLevel.Error,data,callerName,className,lineNr,ob1,obj2);
		}
		
		public static void Error(string data,object ob1,[CallerLineNumber] int lineNr = 0, [CallerMemberName] string callerName = "",[CallerFilePath] string className = "")
		{
			_logger.Write(LogLevel.Error,data,callerName,className,lineNr,ob1);
		}
		
		public static void Error(string data, [CallerLineNumber] int lineNr = 0,[CallerMemberName] string callerName = "",[CallerFilePath] string className = "")
		{
			_logger.Write(LogLevel.Error,data,callerName,className,lineNr);
		}
		#endregion

		#region Critical
		public static void Critical(string data,object ob1,object obj2,object obj3, [CallerLineNumber] int lineNr = 0, [CallerMemberName] string callerName = "",[CallerFilePath] string className = "")
		{
			_logger.Write(LogLevel.Critical,data,callerName,className,lineNr,ob1,obj2,obj3);
		}
		
		public static void Critical(string data,object ob1,object obj2,[CallerLineNumber] int lineNr = 0, [CallerMemberName] string callerName = "",[CallerFilePath] string className = "")
		{
			_logger.Write(LogLevel.Critical,data,callerName,className,lineNr,ob1,obj2);
		}
		
		public static void Critical(string data,object ob1,[CallerLineNumber] int lineNr = 0, [CallerMemberName] string callerName = "",[CallerFilePath] string className = "")
		{
			_logger.Write(LogLevel.Critical,data,callerName,className,lineNr,ob1);
		}
		
		public static void Critical(string data, [CallerLineNumber] int lineNr = 0,[CallerMemberName] string callerName = "",[CallerFilePath] string className = "")
		{
			_logger.Write(LogLevel.Critical,data,callerName,className,lineNr);
		}
		#endregion
	}

	public class LoggerAdapterIsNullException : Exception {
	}
}
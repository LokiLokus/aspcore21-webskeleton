using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using WebSkeleton.Database;
using WebSkeleton.Database.Model;
using WebSkeleton.Filter;
using WebSkeleton.RequestModel;
using WebSkeleton.ResponseModel;
using SignInResult = Microsoft.AspNetCore.Identity.SignInResult;

namespace WebSkeleton.Rest {
	[ApiController]
	[Route("/api/User")]
	[Produces("application/json")]
	[Authorize]
	public class UserController : ControllerBase{
		private readonly DatabaseCxt _dataBaseContext;
		private readonly SignInManager<User> _signInManager;
		private readonly UserManager<User> _userManager;

		public UserController(DatabaseCxt dataBaseContext, UserManager<User> userManager,
			SignInManager<User> signInManager)
		{
			_dataBaseContext = dataBaseContext;
			_userManager = userManager;
			_signInManager = signInManager;
		}
		
		[AllowAnonymous]
		[HttpPut("Login/{id}")]
		public async Task<ActionResult> Login(int id, LoginModel model)
		{
			SignInResult loginResult =
				await _signInManager.PasswordSignInAsync(model.UserName, model.Password, true, true);
			if (loginResult.Succeeded)
				return Ok(RequestResult.Success("Logged In"));
			return BadRequest(loginResult);
		}
	}
}
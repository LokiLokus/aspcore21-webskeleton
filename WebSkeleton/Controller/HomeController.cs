using Microsoft.AspNetCore.Mvc;

namespace WebSkeleton.Controller {
	public class HomeController : Microsoft.AspNetCore.Mvc.Controller{
		public IActionResult Index()
		{
			return View();
		}

		public IActionResult Impressum()
		{
			return View();
		}

		public IActionResult Contact()
		{
			return View();
		}

		public IActionResult Dsgvo()
		{
			return View();
		}

		public IActionResult XR()
		{
			return View();
		}

		public IActionResult App()
		{
			return View();
		}

		public IActionResult Web()
		{
			return View();
		}
	}
}
using System.Collections.Generic;
using WebSkeleton.Filter;

namespace WebSkeleton.ResponseModel {
	public class RequestResult : Dictionary<string,string> {
		public static RequestResult GetResult(string key, string value)
		{
			return new RequestResult()
			{
				{key, value}
			};
		}

		public static RequestResult Success(string value)
		{
			return GetResult("success", value);
		}

		public static RequestResult Error(string value)
		{
			return GetResult("error", value);
		}
	}
}
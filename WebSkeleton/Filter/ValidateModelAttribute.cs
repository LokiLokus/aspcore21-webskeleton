using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace WebSkeleton.Filter {
	public class ValidateModelAttribute : ActionFilterAttribute{
		public override void OnActionExecuting(ActionExecutingContext context)
		{
			if (!context.ModelState.IsValid)
			{
				context.Result = new BadRequestObjectResult(context.ModelState.Values
					.SelectMany(e => e.Errors)
					.Select(e => e.ErrorMessage));
			}
			
			base.OnActionExecuting(context);
		}
	}
}
using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using WebSkeleton.ResponseModel;

namespace WebSkeleton.Filter {
	public class ModelNotNullAttribute : TypeFilterAttribute
	{
		public ModelNotNullAttribute(params object[] input) : base(typeof(NotNullAttrImpl))
		{
			Arguments = input;
		}
		
		private class NotNullAttrImpl : IActionFilter {
			private readonly string _errorMessage;
			public NotNullAttrImpl(object[] ids)
			{
				if (ids == null || ids.Length == 0)
				{
					ids = new object[] {"Attribut cannot be null"};
				}
				_errorMessage = (string)ids[0];
				
			}

			public void OnActionExecuting(ActionExecutingContext context)
			{
				if (context.ActionArguments.Any(x => x.Value == null)) {
					context.Result = new BadRequestObjectResult(RequestResult.Error(_errorMessage));
				}		
				//base.OnActionExecuting(context);
			}

			public void OnActionExecuted(ActionExecutedContext context)
			{
			}
		}
	}
}
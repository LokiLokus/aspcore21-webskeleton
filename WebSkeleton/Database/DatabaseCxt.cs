using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using WebSkeleton.Database.Model;

namespace WebSkeleton.Database {
	public class DatabaseCxt  : IdentityDbContext<User, IdentityRole, string> {
		public DatabaseCxt(DbContextOptions<DatabaseCxt> options)
			: base(options)
		{
			
		}

		public DbSet<User> Users{ get; set; }
		
		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			base.OnModelCreating(modelBuilder);
		}

		protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
		{
			optionsBuilder.UseInMemoryDatabase("Database");
		}
	}
}
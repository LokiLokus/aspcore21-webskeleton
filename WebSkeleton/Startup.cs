﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json.Serialization;
using Swashbuckle.AspNetCore.Swagger;
using WebSkeleton.Database;
using WebSkeleton.Database.Model;
using WebSkeleton.Filter;

namespace WebSkeleton {
	public class Startup {
		public Startup(IConfiguration configuration)
		{
			Configuration = configuration;
		}

		public IConfiguration Configuration { get; }

		// This method gets called by the runtime. Use this method to add services to the container.
		public void ConfigureServices(IServiceCollection services)
		{
			services.AddDbContext<DatabaseCxt>();

			services.AddIdentity<User, IdentityRole>()
				.AddEntityFrameworkStores<DatabaseCxt>();

			services.Configure<IdentityOptions>(options =>
			{
				options.Password.RequireDigit = true;
				options.Password.RequireLowercase = true;
				options.Password.RequireNonAlphanumeric = false;
				options.Password.RequireUppercase = true;
				options.Password.RequiredLength = 6;
				options.Password.RequiredUniqueChars = 1;

				options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(5);
				options.Lockout.MaxFailedAccessAttempts = 5;
				options.Lockout.AllowedForNewUsers = true;

				options.User.AllowedUserNameCharacters =
					"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-.";
				options.User.RequireUniqueEmail = true;
			});

			services.ConfigureApplicationCookie(options =>
			{
				options.Cookie.HttpOnly = true;
				options.ExpireTimeSpan = TimeSpan.FromMinutes(5);

				options.LoginPath = "/Identity/Account/Login";
				options.AccessDeniedPath = "/Identity/Account/AccessDenied";
				options.SlidingExpiration = true;
			});
			
			
			
			
			
			
			services.AddMvc(config =>
				{
					config.Filters.Add(new ValidateModelAttribute());
				})
				.SetCompatibilityVersion(CompatibilityVersion.Version_2_1)
				.AddJsonOptions(options =>
					{
						options.SerializerSettings.ContractResolver =
							new CamelCasePropertyNamesContractResolver();
					});
			services.ConfigureSwaggerGen(options => { options.CustomSchemaIds(x => x.FullName); });
			services.AddSwaggerGen(c =>
			{
				c.SwaggerDoc("v1", new Info {Title = "REST API", Version = "v1"});
			});
		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IHostingEnvironment env, IServiceProvider serviceProvider)
		{
			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
			}
			else
			{
				app.UseHsts();
			}
			app.UseStaticFiles();
			app.UseMvc(routes =>
			{
				routes.MapRoute(
					"areaRoute",
					"{controller=Home}/{action=Index}");
			});
			app.UseAuthentication();
			app.UseSwagger();
			app.UseSwaggerUI(c => { c.SwaggerEndpoint("/swagger/v1/swagger.json", "Contacts API V1"); });
			CreateRoles(serviceProvider);
		}
		
		private void CreateRoles(IServiceProvider serviceProvider)
		{
			RoleManager<IdentityRole> roleManager =
				serviceProvider.GetRequiredService<RoleManager<IdentityRole>>();
			UserManager<User> userManager = serviceProvider.GetRequiredService<UserManager<User>>();
			DatabaseCxt dbContext = serviceProvider.GetRequiredService<DatabaseCxt>();
			foreach (string roleName in new[]
			{
				"Admin", "User"
			})
			{
				Task<bool> hasAdminRole = roleManager.RoleExistsAsync(roleName);
				hasAdminRole.Wait();

				if (hasAdminRole.Result) continue;
				Task<IdentityResult> roleResult = roleManager.CreateAsync(new IdentityRole(roleName));
				roleResult.Wait();
			}

			Task<User> admin = userManager.FindByNameAsync("Admin");
			admin.Wait();
			User adminUser = admin.Result;
			if (admin.Result == null)
			{
				User newUser = new User {UserName = "Admin", Email = "baboo@its2morrow.de"};
				Task<IdentityResult> res =
					userManager.CreateAsync(newUser, "Admin1234");
				res.Wait();

				adminUser = newUser;
			}

			Task<IdentityResult> x = userManager.AddToRoleAsync(adminUser, "Admin");
			x.Wait();
		}
	}
}